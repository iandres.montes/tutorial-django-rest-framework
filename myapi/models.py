from django.db import models

class SuperHeroe(models.Model):
    nombre = models.CharField(max_length=60)
    alias = models.CharField(max_length=60)
    
    def __str__(self):
        return self.nombre