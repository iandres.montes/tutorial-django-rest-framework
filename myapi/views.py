from rest_framework import viewsets

from .serializers import SuperSerializador
from .models import SuperHeroe


class SuperHeroeViewSet(viewsets.ModelViewSet):
    queryset = SuperHeroe.objects.all().order_by('nombre')
    serializer_class = SuperSerializador