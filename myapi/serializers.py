# serializers.py
from rest_framework import serializers

from .models import SuperHeroe

class SuperSerializador(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SuperHeroe
        fields = ('id', 'nombre', 'alias')